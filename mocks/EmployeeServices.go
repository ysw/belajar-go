// Code generated by mockery v2.20.0. DO NOT EDIT.

package mocks

import (
	entity "belajar-go/app/entity"
	context "context"

	mock "github.com/stretchr/testify/mock"
)

// EmployeeServices is an autogenerated mock type for the EmployeeServices type
type EmployeeServices struct {
	mock.Mock
}

// CreateEmployees provides a mock function with given fields: ctx, request
func (_m *EmployeeServices) CreateEmployees(ctx context.Context, request entity.Employees) (interface{}, error) {
	ret := _m.Called(ctx, request)

	var r0 interface{}
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, entity.Employees) (interface{}, error)); ok {
		return rf(ctx, request)
	}
	if rf, ok := ret.Get(0).(func(context.Context, entity.Employees) interface{}); ok {
		r0 = rf(ctx, request)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(interface{})
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, entity.Employees) error); ok {
		r1 = rf(ctx, request)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// FindEmployeeById provides a mock function with given fields: id
func (_m *EmployeeServices) FindEmployeeById(id int) (*entity.Employees, error) {
	ret := _m.Called(id)

	var r0 *entity.Employees
	var r1 error
	if rf, ok := ret.Get(0).(func(int) (*entity.Employees, error)); ok {
		return rf(id)
	}
	if rf, ok := ret.Get(0).(func(int) *entity.Employees); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*entity.Employees)
		}
	}

	if rf, ok := ret.Get(1).(func(int) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

type mockConstructorTestingTNewEmployeeServices interface {
	mock.TestingT
	Cleanup(func())
}

// NewEmployeeServices creates a new instance of EmployeeServices. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewEmployeeServices(t mockConstructorTestingTNewEmployeeServices) *EmployeeServices {
	mock := &EmployeeServices{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
