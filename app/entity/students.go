package entity

type Students struct {
	ID    string `json:"ID"`
	Name  string `json:"Name"`
	Grade int    `json:"Grade"`
}

type MessageResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
