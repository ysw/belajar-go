package entity

type Employees struct {
	Id       int    `json:"id"`
	Fullname string `json:"fullname" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Age      int    `json:"age" validate:"gte=0,lte=130"`
	Division string `json:"division" validate:"required"`
	Golongan string `json:"golongan" validate:"required"`
}
