package handler

import (
	"belajar-go/app/services/students"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

func GetUsers(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	res, err := students.GetStudents()

	BuildResponse(w, res, err)

}
