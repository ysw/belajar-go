package handler

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
)

type handlerEmployee interface {
	CreateEmployees(w http.ResponseWriter, r *http.Request, _ httprouter.Params)
	FindEmployeeById(w http.ResponseWriter, r *http.Request, params httprouter.Params)
}
