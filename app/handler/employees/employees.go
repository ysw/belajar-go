package employees

import (
	"belajar-go/app/entity"
	"belajar-go/app/handler"
	"belajar-go/app/services"
	pkg "belajar-go/pkg/common"
	hlp "belajar-go/pkg/helper"
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
)

type HTTP struct {
	CategoryService services.EmployeeServices
}

func NewHTTP(categoryService services.EmployeeServices) *HTTP {
	return &HTTP{CategoryService: categoryService}
}

func (h HTTP) CreateEmployees(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var (
		employee = entity.Employees{}
		err      error
		res      interface{}
		validate *validator.Validate
	)

	logger := hlp.GetLogger(r.Context())

	defer func() {
		if err != nil {
			logger.WithField("Exception Caught", err).Error("Handler")
		}

		handler.BuildResponse(w, res, err)
	}()

	decoder := json.NewDecoder(r.Body)

	if err = decoder.Decode(&employee); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	validate = validator.New()

	err = validate.Struct(employee)
	if err != nil {
		err = hlp.Error(err, pkg.ErrMissingParamater, err.Error())
		return
	}

	logger.WithField("Body Request", employee).Info("Handler")

	if res, err = h.CategoryService.CreateEmployees(r.Context(), employee); err != nil {
		return
	}

	logger.WithField("Body Response", res).Info("Handler")

	res = map[string]interface{}{"code": pkg.Success, "message": res}

}

func (h HTTP) FindEmployeeById(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var (
		id  int
		err error
	)

	employeeId := params.ByName("employeeId")
	id, err = strconv.Atoi(employeeId)

	res, err := h.CategoryService.FindEmployeeById(id)

	defer func() {
		handler.BuildResponse(w, res, err)
	}()

}
