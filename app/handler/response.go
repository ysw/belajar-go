package handler

import (
	"belajar-go/app/entity"
	pkg "belajar-go/pkg/common"
	hlp "belajar-go/pkg/helper"
	"encoding/json"
	"net/http"
)

func BuildResponse(w http.ResponseWriter, res interface{}, err error) {

	response := entity.MessageResponse{}

	if err != nil {
		response.Status, response.Message = hlp.ErrorCodeAndMessage(err)
	} else {
		response.Status = pkg.Success
		response.Message = "Success"
		response.Data = res
	}

	w.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(w).Encode(response)
	PanicIfError(err)
}

func PanicIfError(err error) {
	if err != nil {
		panic(err)
	}
}
