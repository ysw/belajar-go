package middleware

import (
	"belajar-go/app/entity"
	"belajar-go/app/handler"
	pkg "belajar-go/pkg/helper"
	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
	"net/http"
)

type AuthMiddleware struct {
	Handler *httprouter.Router
}

func NewAuthMiddleware(handler *httprouter.Router) *AuthMiddleware {
	return &AuthMiddleware{Handler: handler}
}

func (a AuthMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if "RAHASIA" == r.Header.Get("X-API-Key") {
		// ok
		requestId := r.Header.Get("X-RequestID")

		ctx := pkg.SetLogger(r.Context(), log.NewEntry(log.StandardLogger()), "belajar-go", requestId)

		a.Handler.ServeHTTP(w, r.WithContext(ctx))
	} else {
		// error
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)

		webResponse := entity.MessageResponse{
			Status:  http.StatusUnauthorized,
			Message: "UNAUTHORIZED",
		}

		handler.BuildResponse(w, webResponse, nil)

		return
	}

}
