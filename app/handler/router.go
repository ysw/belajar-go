package handler

import (
	"github.com/julienschmidt/httprouter"
)

func Router(employeeController handlerEmployee) *httprouter.Router {
	router := httprouter.New()
	router.GET("/getUsers", GetUsers)
	router.POST("/employee/create", employeeController.CreateEmployees)
	router.GET("/employee/:employeeId", employeeController.FindEmployeeById)
	return router
}
