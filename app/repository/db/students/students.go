package students

import (
	"belajar-go/app/entity"
	cm "belajar-go/pkg/common"
	pkg "belajar-go/pkg/database"
	hlp "belajar-go/pkg/helper"
)

func InsertStudentsDB(students []entity.Students) (err error) {

	db, err := pkg.Connect()

	if err != nil {
		return hlp.Error(err, cm.ErrDatabaseConnection, err.Error())
	}
	defer db.Close()

	for _, student := range students {

		_, err = db.Exec("insert into students values (?, ?, ?)", student.ID, student.Name, student.Grade)

		if err != nil {
			return hlp.Error(err, cm.ErrDatabaseQuery, err.Error())
		}

	}
	return
}
