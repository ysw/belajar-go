package employee

import (
	"belajar-go/app/entity"
	cm "belajar-go/pkg/common"
	hlp "belajar-go/pkg/helper"
	"database/sql"
)

func (e EmployeeRepository) FindEmployeeById(id int, db *sql.DB) (result *entity.Employees, err error) {
	var employee = entity.Employees{}

	if err != nil {
		return nil, hlp.Error(err, cm.ErrDatabaseConnection, "")
	}

	rows, err := db.Query("select employeeID, employeeName, email,age, division from employees where employeeID = ?", id)
	if err != nil {
		return nil, hlp.Error(err, cm.ErrDatabaseQuery, "")
	}
	defer rows.Close()
	for rows.Next() {

		err = rows.Scan(&employee.Id, &employee.Fullname, &employee.Email, &employee.Age, &employee.Division)

		if err != nil {
			hlp.Error(err, cm.ErrDatabaseQuery, "")
		}
	}

	result = &employee
	return
}
