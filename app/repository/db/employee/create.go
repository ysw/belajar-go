package employee

import (
	"belajar-go/app/entity"
	cm "belajar-go/pkg/common"
	hlp "belajar-go/pkg/helper"
	log "belajar-go/pkg/helper"
	"context"
	"database/sql"
)

type EmployeeRepository struct {
}

func NewCategoryRepositoryImpl() *EmployeeRepository {
	return &EmployeeRepository{}
}

func (e EmployeeRepository) CreateEmployeeDB(ctx context.Context, emp entity.Employees, db *sql.DB) (err error) {

	var (
		stmt *sql.Stmt
	)

	logger := log.GetLogger(ctx)

	defer func() {
		if err != nil {
			logger.WithField("Exception cought", err).Error("Handler")
		}
	}()

	query := `insert into employees (employeeName,email,age,division) values (?,?,?,?)`

	if stmt, err = db.Prepare(query); err != nil {
		return hlp.Error(err, cm.ErrDatabaseQuery, "")
	}

	if _, err = stmt.Exec(emp.Fullname, emp.Email, emp.Age, emp.Division); err != nil {
		return hlp.Error(err, cm.ErrDatabaseQuery, "")
	}

	return
}
