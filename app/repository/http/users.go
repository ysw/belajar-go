package http

import (
	"belajar-go/app/entity"
	pkg "belajar-go/pkg/common"
	hlp "belajar-go/pkg/helper"
	"encoding/json"
	"net/http"
)

func GetAndInsertUsers() (students []entity.Students, err error) {
	var (
		client = &http.Client{}
	)

	request, e := http.NewRequest("POST", pkg.BaseURL+"/users", nil)

	if e != nil {
		return nil, hlp.Error(err, pkg.ErrHttpRequest, "Request error")
	}

	request.Header.Set("Content-Type", "application/json")

	response, e := client.Do(request)
	if e != nil {
		return nil, hlp.Error(err, pkg.ErrHttpRequest, "Request error")
	}

	defer response.Body.Close()

	e = json.NewDecoder(response.Body).Decode(&students)
	if e != nil {
		return nil, e
	}

	return
}
