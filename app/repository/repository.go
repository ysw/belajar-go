package repository

import (
	"belajar-go/app/entity"
	"context"
	"database/sql"
)

type EmployeeRepository interface {
	CreateEmployeeDB(ctx context.Context, emp entity.Employees, DB *sql.DB) (err error)
	FindEmployeeById(id int, DB *sql.DB) (result *entity.Employees, err error)
	GetGolongan(id string) (result *entity.Employees, err error)
}
