package services

import (
	"belajar-go/app/entity"
	"context"
)

type EmployeeServices interface {
	CreateEmployees(ctx context.Context, request entity.Employees) (result interface{}, err error)
	FindEmployeeById(id int) (result *entity.Employees, err error)
	GetGolongan(id string) (result *entity.Employees, err error)
}
