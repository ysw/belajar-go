package students

import (
	"belajar-go/app/entity"
	repo "belajar-go/app/repository/db/students"
	api "belajar-go/app/repository/http"
)

func GetStudents() (result interface{}, err error) {
	var (
		students []entity.Students
	)

	if students, err = api.GetAndInsertUsers(); err != nil {
		return nil, err
	}

	if err = repo.InsertStudentsDB(students); err != nil {
		return nil, err
	}

	result = students

	return
}
