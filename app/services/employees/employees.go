package employees

import (
	"belajar-go/app/repository"
	"database/sql"
)

type EmployeeServiceImpl struct {
	repo repository.EmployeeRepository
	DB   *sql.DB
}

func NewCategoryServiceImpl(repo repository.EmployeeRepository, DB *sql.DB) *EmployeeServiceImpl {
	return &EmployeeServiceImpl{repo: repo, DB: DB}
}
