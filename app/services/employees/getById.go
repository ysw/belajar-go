package employees

import (
	"belajar-go/app/entity"
	cm "belajar-go/pkg/common"
	hlp "belajar-go/pkg/helper"
)

func (c EmployeeServiceImpl) FindEmployeeById(id int) (result *entity.Employees, err error) {

	var response *entity.Employees
	if response, err = c.repo.FindEmployeeById(id, c.DB); err != nil {
		return nil, err
	}

	if response.Fullname == "" {
		err = hlp.Error(nil, cm.ErrDataNotFound, "Data not found in database")
	}

	result = response
	return
}
