package employees

import (
	"belajar-go/app/entity"
	log "belajar-go/pkg/helper"
	"context"
)

func (c EmployeeServiceImpl) CreateEmployees(ctx context.Context, request entity.Employees) (result interface{}, err error) {

	logger := log.GetLogger(ctx)

	defer func() {
		if err != nil {
			logger.WithField("Exception cought", err).Error("Service")
		}
	}()

	if err = c.repo.CreateEmployeeDB(ctx, request, c.DB); err != nil {
		return nil, err
	}

	result = request.Fullname

	logger.WithField("Result from repository", result).Info("Service")

	return
}
