package employees

import (
	"belajar-go/app/entity"
	"belajar-go/mocks"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

var (
	db, _, _           = sqlmock.New()
	employeeRepository = &mocks.EmployeeRepository{mock.Mock{}}
	employeeService    = EmployeeServiceImpl{DB: db, repo: employeeRepository}
)

func TestGetById(t *testing.T) {
	employee := &entity.Employees{
		Id:       "1",
		Fullname: "testing 1",
		Age:      24,
		Email:    "testing@mail.com",
		Division: "Testing",
	}

	employeeRepository.Mock.On("FindEmployeeById", 2, db).Return(employee)

	result, err := employeeService.FindEmployeeById(2)
	assert.Nil(t, err)
	assert.NotNil(t, result)
	assert.Equal(t, employee.Id, result.Id)
	assert.Equal(t, "testing 2", result.Fullname)
}
