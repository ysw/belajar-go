package main

import (
	router "belajar-go/app/handler"
	handler "belajar-go/app/handler/employees"
	"belajar-go/app/handler/middleware"
	repository "belajar-go/app/repository/db/employee"
	service "belajar-go/app/services/employees"
	pkg "belajar-go/pkg/database"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05.999",
	})

}

func main() {

	db, err := pkg.Connect()
	if err != nil {
		panic(err)
	}
	employeeRepository := repository.NewCategoryRepositoryImpl()
	employeeService := service.NewCategoryServiceImpl(employeeRepository, db)
	employeeHandler := handler.NewHTTP(employeeService)
	router := router.Router(employeeHandler)

	server := http.Server{
		Addr:    "localhost:3000",
		Handler: middleware.NewAuthMiddleware(router),
	}

	e := server.ListenAndServe()
	fmt.Println("Server Running")

	if e != nil {
		panic(e)
	}

}
