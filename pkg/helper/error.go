package helper

import (
	"errors"
	"fmt"
	"runtime"
	"strconv"
	"strings"
)

var ErrGeneral = 31

type ApplicationError struct {
	msg   string
	code  string
	cause error
}

func (e *ApplicationError) getErrorMessage() string {
	return e.msg
}

func (e *ApplicationError) Error() string {
	msg := e.getErrorMessage()
	if e.code != "" {
		msg = fmt.Sprintf("%s (type: %s)", msg, e.code)
	}
	if e.cause != nil {
		msg = fmt.Sprintf("%s: %v", msg, e.cause)
	}
	return msg
}

func (e *ApplicationError) getErrorCode() string {
	return e.code
}

func NewApplicationError(msg string, code string, cause error) error {
	applicationErr := &ApplicationError{
		msg:   msg,
		code:  code,
		cause: cause,
	}

	return applicationErr
}

func SetApplicationError(errMessage, errCode string) error {
	return NewApplicationError(errMessage, errCode, nil)
}

func SetApplicationErrorWithCause(errMessage, errCode string, cause error) error {
	return NewApplicationError(errMessage, errCode, cause)
}

// Error ...
func Error(err error, errCode int, errMessage string) error {

	if _, file, line, ok := runtime.Caller(1); ok {
		f := strings.Split(file, "/")
		errMessage = fmt.Sprintf("%s/%s:%d", errMessage, f[len(f)-1], line)
	}
	if err == nil {
		return SetApplicationError(errMessage, ErrorCodeString(errCode))
	}
	return SetApplicationErrorWithCause(errMessage, ErrorCodeString(errCode), err)
}

func ErrorCodeString(errCode int) string {
	return fmt.Sprintf("%d", errCode)
}

// ErrorMessage ...
func ErrorMessage(err error) string {
	return err.Error()
}

// ErrorMessage ...
func ErrorCode(err error) int {

	var applicationErr *ApplicationError

	if errors.As(err, &applicationErr) {
		code, _ := strconv.Atoi(applicationErr.getErrorCode())
		return code

	}

	return ErrGeneral
}

// ErrorCodeAndMessage ...
func ErrorCodeAndMessage(err error) (code int, remark string) {
	return ErrorCode(err), ErrorMessage(err)
}
