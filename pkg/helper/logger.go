package helper

import (
	"context"
	log "github.com/sirupsen/logrus"
)

const contextLogger = "logger"

func SetLogger(ctx context.Context, entry *log.Entry, appName, traceID string) context.Context {
	return context.WithValue(
		ctx,
		contextLogger,
		entry.WithFields(log.Fields{"app": appName, "msg_id": traceID}),
	)
}

func GetLogger(ctx context.Context) *log.Entry {
	entry, _ := ctx.Value(contextLogger).(*log.Entry)
	if entry == nil {
		entry = log.NewEntry(log.StandardLogger())
	}

	return entry

}
