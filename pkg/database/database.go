package database

import "database/sql"

func Connect() (*sql.DB, error) {
	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3306)/belajar_golang_restful_api")
	if err != nil {
		return nil, err
	}

	return db, nil
}
