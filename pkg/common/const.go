package common

var BaseURL = "http://localhost:8080"

const (
	Success = 0
	Failed  = 1
)

const (
	ErrDataNotFound       = 20
	ErrMissingParamater   = 21
	ErrDatabaseConnection = 30
	ErrDatabaseQuery      = 31
	ErrHttpRequest        = 32
	ErrTimeOut            = 33
	ErrGeneral            = 90
)
